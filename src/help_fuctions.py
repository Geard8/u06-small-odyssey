from sqlite3 import IntegrityError, connect, Error

def create_connection(db_file):
    """ 
    create a database connection to the SQLite database specified by db_file
    
    param: 
        db_file: database file
    return: Connection object or None
    """
    conn = None
    try:
        conn = connect(db_file)
        return conn
    except Error as e:
        print(e)

    return conn


def user_number_choice(text, choice_list, zero_choice = False):
    """ 
    Print out text followed by numberd list based on choice_list starting from 1 and if a zero_choice 
    exist, it will be added at the end of numberd list as 0.
    Then take input from user and repeat until input match one choice and then return that number.
    
    param: 
        text: string that will be printed out.
        choice_list: list of strings that will be printed out in order as part of a a numberd list.
        zero_choice: OPTIONAL string that will be printed out last in numberd list as 0.
    return: int that is user valid choice.
    """
    while True:
        print(text)
        number_list_count = 1
        for choice in choice_list:
            print(f"{number_list_count}. {choice}")
            number_list_count += 1

        if zero_choice:
            print(f"0. {zero_choice}")

        user_input = input("Select your choice by number: ")

        try:
            user_input = int(user_input)
        except ValueError:
            print('\nNot a number. Please enter valid number like 1.\n')
            continue
        
        # Check if user_input is a valid choice
        if (user_input < number_list_count and number_list_count >= 1) or (user_input == 0 and zero_choice):
            return user_input # return valid user_input
        else:
            print('\nNot a valid choice. Please enter valid number.\n')
            continue
            

def user_select_id():
    """ 
    User will select a positive number.
    return: int that a positive number.
    """
    while True:
        user_input = input("Select a positive number: ")

        try:
            user_input = int(user_input)
        except ValueError:
            print('\nNot a number. Please enter valid number like 1.\n')
            continue
        
        # Check if user_input is a valid choice
        if (user_input > 0):
            return user_input # return valid user_input
        else:
            print('\nNot a valid choice. Please enter valid number.\n')
            continue


def check_id_exist(id, db_conn, table_name):
    """ 
    Will check if id is in table table_name from db_conn. Retruns True or False

    param: 
        id: what id will be deleted
        db_conn: the Connection object
        table_name: table name 
    return: True if id exist in table table_name otherwise it returns False
    """
    cur = db_conn.cursor()
    cur.execute("SELECT * FROM '%s' WHERE id=%s" % (table_name, id))
    id_exist = bool(cur.fetchall())
    return id_exist


def select_all_from_table(db_conn, table_name):
    """
    Query all rows in the table_name table
    param:
        db_conn: the Connection object
        table_name: table name 
    return:
        rows: all rows from table_name
        columns_name: all column name table_name
    """
    cur = db_conn.cursor()
    cur.execute("SELECT * FROM '%s'" % table_name)
    columns_name = list(map(lambda x: x[0], cur.description))
    rows = cur.fetchall()

    return rows, columns_name


def delete_one_row(id, db_conn, table_name):
    """
    Will delete one row from table table_name from db_conn with id.

    param: 
        id: what id will be deleted
        db_conn: the Connection object
        table_name: table name 
    return: True if all went well.
    """
    cur = db_conn.cursor()
    cur.execute("DELETE FROM '%s' WHERE id='%s'" % (table_name, id))
    db_conn.commit()

    return True


def create_one_row(db_conn, table_name):
    """
    Will create one row from table table_name.
    All table_name have diffrent collums and will ask user for input based in what it needs for that table_name.

    param:
        db_conn: the Connection object
        table_name: table name 
    return: True if all went well.
    """
    if table_name == "Location":
        name_input = input("what unique name shall new location have: ")
        text_input = input("what text shall new location have: ")

        try:
            cur = db_conn.cursor()
            cur.execute("INSERT INTO Location(name,text) VALUES(?, ?)", (name_input, text_input))
            db_conn.commit()
        except IntegrityError:
            print("\nLocation already exist by that name. No new location has been created.\n")
            return False

    elif table_name == "Choice":
        choice_input = input("What the choice shall be with text: ")
        text_input = input("What text shall show when this choice is selected by the player: ")
        print("select what location this choice is for by id")
        location_id_input = user_select_id()

        if not check_id_exist(location_id_input, db_conn, "Location"):
            print(f"\n A location with id location_id_input do not exist. No new choice has been created.\n")
            return False

        try:
            cur = db_conn.cursor()
            cur.execute("INSERT INTO Choice(choice,text,location_id) VALUES(?, ?, ?)", (choice_input, text_input, location_id_input))
            db_conn.commit()
        except IntegrityError:
            print("\nChoice already exist by that name. No new choice has been created.\n")
            return False

    elif table_name == "ActionChangeLocation":
        print("select what choice by id that shall change the players location")
        choice_id_input = user_select_id()
        
        if not check_id_exist(choice_id_input, db_conn, "Choice"):
            print(f"\n A location with id location_id_input do not exist. No new ActionChangeLocation has been created.\n")
            return False

        print("select what location to change to when for this choice :")
        location_id_input = user_select_id()
        if not check_id_exist(location_id_input, db_conn, "Location"):
            print(f"\n A location with id location_id_input do not exist. No new ActionChangeLocation has been created.\n")
            return False
        
        try:
            cur = db_conn.cursor()
            cur.execute("INSERT INTO ActionChangeLocation(choice_id,location_id) VALUES(?, ?)", (choice_id_input, location_id_input))
            db_conn.commit()
        except IntegrityError:
            print("\ActionChangeLocation already exist by that name. No new ActionChangeLocation has been created.\n")
            return False

    else:
        raise Exception(f"error: {table_name} do not match any tables name that user can create row in")

    return True


def update_table_row(db_conn, table_name):
    """
    Allow user to change one string for table "Location" or "Choice".

    param: 
        db_conn: the Connection object
        table_name: table name 
    return: True if something was change or False if nothing was change.
    """

    print(f"select id for {table_name} what you wish to change")
    id_input = user_select_id()

    if not check_id_exist(id_input, db_conn, table_name):
        print(f"\n A {table_name} with id {id_input} do not exist. Nothing has been changed.\n")
        return False

    if table_name == "Location":
        ask_player_text = "What will you be changing for this Location"
        collum_list_to_be_changed = ("name", "text")
        stop_text = "Stop and change nothing"
        user_input = user_number_choice(ask_player_text, collum_list_to_be_changed, stop_text)

        # name choice
        if user_input == 1:
            player_text_input = input(f"What new name shall the selcted {table_name} have: ")

            cur = db_conn.cursor()
            cur.execute("UPDATE Location SET name = ? WHERE id = ?", (player_text_input, id_input))
            db_conn.commit()

        # change text
        elif user_input == 2:
            player_text_input = input(f"What new text shall the selcted {table_name} have: ")

            cur = db_conn.cursor()
            cur.execute("UPDATE Location SET text = ? WHERE id = ?", (player_text_input, id_input))
            db_conn.commit()

        else:
            return False

    elif table_name == "Choice":
        ask_player_text = "What will you be changing for this Choice"
        collum_list_to_be_changed = ("choice", "text")
        stop_text = "Stop and change nothing"
        user_input = user_number_choice(ask_player_text, collum_list_to_be_changed, stop_text)

        # change choice
        if user_input == 1:
            player_text_input = input(f"What new choice shall the selcted {table_name} have: ")

            cur = db_conn.cursor()
            cur.execute("UPDATE Choice SET choice = ? WHERE id = ?", (player_text_input, id_input))
            db_conn.commit()

        # change text
        elif user_input == 2:
            player_text_input = input(f"What new text shall the selcted {table_name} have: ")

            cur = db_conn.cursor()
            cur.execute("UPDATE Choice SET text = ? WHERE id = ?", (player_text_input, id_input))
            db_conn.commit()

        else:
            return False

    return True


def work_on_table_loop(db_conn, table_name):
    """
    User working on a table core loop as part of creating a small odyssey.
    Here the user can add, change and delete from table_name and get help.

    param: 
        db_conn: the Connection object
        table_name: table name 
    return: True if all went well.
    """

    while True:
        text = f"What do you want to do with {table_name}"
        choice_list = ["help", "view all", "create new", "remove one"]

        # if table_name is Location or Choice it will allow user to change
        if table_name == "Location" or table_name == "Choice":
            choice_list.append("change one")
        stop_text = f"stop working on {table_name}"
        user_input = user_number_choice(text, choice_list, stop_text)

        if user_input == 0:
            return True

        # user selected help
        elif user_input == 1:
            print(f"\nTODO low prio nice to have. Here it will print out text to help and explain what {table_name} do.\n")
        
        # user selected view all
        elif user_input == 2:
            rows, columns_name = select_all_from_table(db_conn, table_name)

            print("") # empty line make it clear where the first row start when reading.

            for row in rows:
                collum_content_list = map(lambda x, y: str(x) + ": " + str(y), columns_name, row)
                for collum_content in collum_content_list:
                    print(collum_content)
                print("") # empty line to sepearet the rows
            
        # user selected create new
        elif user_input == 3:
            create_one_row(db_conn, table_name)
        
        # user selected remove one
        elif user_input == 4:
            user_input_id = user_select_id()

            # id 1 for location is the starting location and the game can't work without.
            # So here is will check and tell user that it can't be deleted.
            if table_name == "Location" and user_input_id == 1:
                print("id 1 from Location is the starting location and can not be deleted")
                continue

            id_exist = check_id_exist(user_input_id, db_conn, table_name)
            if id_exist:
                delete_one_row(user_input_id, db_conn, table_name)
                print(f"\nid {user_input_id} has been deleted from {table_name}\n")
            else:
                print(f"\nid {user_input_id} do not exist.\n")
        
        # user selected change one
        elif user_input == 5:
            update_table_row(db_conn, table_name)

def get_player_location(db_conn):
    """ 
    Get and return current player location.

    param: 
        db_conn: the Connection object
    return: current player location.
    """
    cur = db_conn.cursor()
    cur.execute("SELECT Location.id, name, text FROM Location INNER JOIN PlayerLocation on PlayerLocation.location_id = Location.id")
    player_location = cur.fetchone()

    return player_location


def get_all_choice_for_one_location(db_conn, location_id):
    """ 
    Get and return all choice for one location based on location id.

    param: 
        db_conn: the Connection object
        location_id: id for a location.
    return: all choice for one location.
    """
    cur = db_conn.cursor()
    cur.execute("SELECT * FROM Choice WHERE location_id = '%s'" % (location_id))
    list_of_chocie = cur.fetchall()

    return list_of_chocie


def change_player_location_based_on_choice(db_conn, choice_id):
    """ 
    If this choice shall change player location by having a ActionChangeLocation, then 
    change player location to new location for that choice and return True.
    If this choice do not change player location, then nothing happens and return False.

    param: 
        db_conn: the Connection object
        choice_id: id for a choice
    return: True if locaiton has change, else return False.
    """
    cur = db_conn.cursor()
    cur.execute("SELECT * FROM ActionChangeLocation WHERE choice_id=%s" % (choice_id))
    action_change_location = cur.fetchone()
    exist_action_change_location = bool(action_change_location)

    # if choice dont change location do nothing and retrun False     
    if not exist_action_change_location:
        return False

    # else choice do change location so will update PlayerLocation to new location and return True
    else:
        new_location_id = str(action_change_location[1])
        cur = db_conn.cursor()
        cur.execute("UPDATE PlayerLocation SET location_id = ?", (new_location_id))
        db_conn.commit()

        return True
