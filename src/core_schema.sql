-- Use PRAGMA to turn foreign keys on (off by default)
PRAGMA foreign_keys=ON;

--This is to create the basic database setup ment for creating a small odyssey game.
BEGIN TRANSACTION;
-- Create table for location, that will be used as all kind of location like a city, house, room, part of room like bookshelf, a person or anything fit to have many Choice.
CREATE TABLE Location (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    name TEXT NOT NULL UNIQUE, 
                    text TEXT NOT NULL);
-- Add a starting location that game creater later can change what it is.
INSERT INTO Location (id, name, text) VALUES (1, "change location name of starting location", "Change this to whatever you want the starting location text to be");
-- Create table for PlayerLocation that will track at what Location the player is.
CREATE TABLE PlayerLocation (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    location_id INTEGER NOT NULL,
                    FOREIGN KEY(location_id) REFERENCES Location(id));
-- Add so that player start at starting location.
INSERT INTO PlayerLocation (location_id) VALUES (1);
-- Create table for Choice what will contain all the diffrent choice that player can make and at what location it can be made.
CREATE TABLE Choice (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    choice TEXT NOT NULL,
                    text TEXT NOT NULL, 
                    location_id INTEGER NOT NULL,
                    FOREIGN KEY(location_id) REFERENCES Location(id));
-- Create table for ActionChangeLocation what will be used to know what Choice will trigger that the PlayerLocation will change to specific Location
CREATE TABLE ActionChangeLocation (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                    location_id INTEGER NOT NULL,
                    choice_id INTEGER NOT NULL,
                    FOREIGN KEY(location_id) REFERENCES Location(id)
                    FOREIGN KEY(choice_id) REFERENCES Choice(id));

COMMIT;
