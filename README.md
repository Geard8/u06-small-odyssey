# u06 Small Odyssey

School project part of education for DevOps.
This project is a python based project that use database that will be half way done and then all student will give and receive the project from different student to finish the other half of the project.

## Getting started

create_small_odyssey_game.py is for creating a small odyssey game. Will will create a file my_game.sog or use my_game.sog if it already exist.

my_game.sog is the file of the game. If you want to start creating a new game you need to delete my_game.sog(make a backup, copy or rename with a diffrent name if you wish to save it). sog stand for "small odyssey game"

play_small_odyssey_game.py is for playing a a small odyssey game. Will check for a my_game.sos file and countinu the game with it if it exist, if my_game.sos don't exist it will check for a my_game.sog file and game a new my_game.sos based on my_game.sog.

my_game.sog is a save file for a small odyssey game. If you want to start a new game you need to delete my_game.sog(make a backup, copy or rename with a diffrent name if you wish to save it for later). sos stand for "small odyssey save"

## Extra document and more

Here his a PDF of a goggle doc used during the project for thing like documenting the database(both model and purpus), sprint planning and task tracking, progress reports of what has been work on when, project concept, goal and more gatherd in one place even if its a bit unorganized it is small and mangable and make use nothing import get lost the has been writen down in it.
[PDF of docment used during project](https://gitlab.com/Geard8/u06-small-odyssey/-/blob/main/document%20samling%202022-05-01.pdf)
This is the version for end of sprint 1